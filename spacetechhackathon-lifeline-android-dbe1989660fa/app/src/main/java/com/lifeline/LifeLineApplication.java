package com.lifeline;

import android.app.Application;
import android.content.Context;

import com.lifeline.Service.BackendlessService;

/**
 * Created by jacob on 10/30/17.
 */

public class LifeLineApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        LifeLineApplication.context = getApplicationContext();
        BackendlessService.initApplication(getAppContext());
    }

    public static Context getAppContext() {
        return LifeLineApplication.context;
    }
}
