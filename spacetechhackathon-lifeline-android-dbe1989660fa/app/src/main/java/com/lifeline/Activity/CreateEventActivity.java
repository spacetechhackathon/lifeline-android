package com.lifeline.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.util.Calendar;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.lifeline.R;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import com.lifeline.LifeLineApplication;
import com.lifeline.Service.BackendlessService;
import com.backendless.async.callback.AsyncCallback;



import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import in.goodiebag.carouselpicker.CarouselPicker;


public class CreateEventActivity extends AppCompatActivity{
    EditText mDateEditText;

    BackendlessService backendlessService = BackendlessService.getInstance();

    CarouselPicker carouselPicker,carouselPicker2;

    private String key_doctor = "9E816C76-71FD-7747-FFB3-9BEE7CD9E200";
    private String key_home = "C64DFDDE-2DCA-477D-FFC2-D5581D2E4E00";

    Button btn;

    String events_name = "Go to Hospital";
    String fireDate;

    String startLocationObjId = "58.365798,26.690845";
    String endLocationObjId = "58.367145,26.693172";

    String email = "user@gmail.com";
    String password = "12345";
    View focusView = null;

    final AsyncCallback<Object> callback = new AsyncCallback<Object>() {
        @Override
        public void handleResponse(Object response) {
            System.out.println( response );
        }
        @Override
        public void handleFault(BackendlessFault fault) {
            System.out.println( fault );
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);



        final TimePicker timePicker = (TimePicker)findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

  //      InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
   //     imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);



        btn = (Button)findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDateEditText = (EditText)findViewById(R.id.txtDateEntered);

                String dates = mDateEditText.getText().toString();

                String[] time;
                String delimiter = "-";

                time = dates.split(delimiter);

                String day = time[0];
                String month = time[1];
                String year = time[2];


                int iHour,iMinute;
                iHour = timePicker.getHour();
                iMinute = timePicker.getMinute();

                String sHour,sMinute;
                sHour = String.valueOf(iHour);
                sMinute = String.valueOf(iMinute);

                fireDate = year + "-" + month + "-" + day;

                backendlessService.createNewEventWithLocationIDsAsync(events_name, fireDate,key_home,key_doctor, callback);
                Toast.makeText(CreateEventActivity.this, day+"-"+month+"-"+year, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(LifeLineApplication.getAppContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);


            }
        });


        carouselPicker = (CarouselPicker)findViewById(R.id.carouselpicker);
        List<CarouselPicker.PickerItem> textItem = new ArrayList<>();
        textItem.add(new CarouselPicker.TextItem(" ",8));
        textItem.add(new CarouselPicker.TextItem("Doctor",8));
        textItem.add(new CarouselPicker.TextItem("Chiropractor",8));
        textItem.add(new CarouselPicker.TextItem("Hospital",8));
        textItem.add(new CarouselPicker.TextItem("Home",8));
        textItem.add(new CarouselPicker.TextItem("Pharmacy",8));


        CarouselPicker.CarouselViewAdapter textAdapter = new CarouselPicker.CarouselViewAdapter(this,textItem,0);
        carouselPicker.setAdapter(textAdapter);

        carouselPicker2 = (CarouselPicker)findViewById(R.id.carouselpicker2);
        List<CarouselPicker.PickerItem> textItem2 = new ArrayList<>();
        textItem2.add(new CarouselPicker.TextItem(" ",8));
        textItem2.add(new CarouselPicker.TextItem("Doctor",8));
        textItem2.add(new CarouselPicker.TextItem("Chiropractor",8));
        textItem2.add(new CarouselPicker.TextItem("Hospital",8));
        textItem2.add(new CarouselPicker.TextItem("Home",8));
        textItem2.add(new CarouselPicker.TextItem("Pharmacy",8));

        CarouselPicker.CarouselViewAdapter textAdapter2 = new CarouselPicker.CarouselViewAdapter(this,textItem2,0);
        carouselPicker2.setAdapter(textAdapter2);



    }


}
