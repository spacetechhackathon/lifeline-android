package com.lifeline.Activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.lifeline.Fragment.AddLocationDialogFragment;

import com.lifeline.Fragment.EventsFragment;
import com.lifeline.Fragment.LocationsFragment;
import com.lifeline.LifeLineApplication;
import com.lifeline.Model.Events;
import com.lifeline.Model.Locations;
import com.lifeline.R;

public class MainActivity extends AppCompatActivity implements LocationsFragment.OnListFragmentInteractionListener,
        AddLocationDialogFragment.AddLocationFragmentListener, EventsFragment.OnEventsFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private TabLayout mTabLayout;

    FloatingActionButton fab,fab2,fab3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);




        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                AddLocationDialogFragment newFragment = new AddLocationDialogFragment();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
            }
        });
        ;
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LifeLineApplication.getAppContext(), CreateEventActivity.class);
                startActivity(intent);
            }
        });

        final Animation mShowButton = AnimationUtils.loadAnimation(MainActivity.this,R.anim.show_button);
        final Animation mHideButton = AnimationUtils.loadAnimation(MainActivity.this,R.anim.hide_button);
        final Animation mShowLayout = AnimationUtils.loadAnimation(MainActivity.this,R.anim.show_layout);
        final Animation mHideLayout = AnimationUtils.loadAnimation(MainActivity.this,R.anim.hide_layout);


        fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((fab.getVisibility() == View.VISIBLE) && (fab2.getVisibility() == View.VISIBLE)){
                    fab.setVisibility(View.GONE);
                    fab2.setVisibility(View.GONE);
                    fab.startAnimation(mHideLayout);
                    fab2.startAnimation(mHideLayout);
                    fab3.startAnimation(mHideButton);

                }else{
                    fab.setVisibility(View.VISIBLE);
                    fab2.setVisibility(View.VISIBLE);
                    fab.startAnimation(mShowLayout);
                    fab2.startAnimation(mShowLayout);
                    fab3.startAnimation(mShowButton);

                }

            }
        });


    }

    @Override
    public void onListFragmentInteraction(Locations item){
        //you can leave it empty
    }

    @Override
    public void onEventsFragmentInteraction(Events item){
        //you can leave it empty
    }

    @Override
    public void onFragmentDismissed(){
        LocationsFragment locationsFragment = (LocationsFragment)mSectionsPagerAdapter.getItem(0);
        locationsFragment.reloadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private Fragment mLocationsFragment = new LocationsFragment();
        private Fragment mEventsFragment = new EventsFragment();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return mLocationsFragment;
                case 1:
                    return mEventsFragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "LOCATIONS";
                case 1:
                    return "EVENTS";
            }
            return null;
        }
    }
}
