package com.lifeline.Model;

/**
 * Created by jacob on 10/31/17.
 */

public class Events
{
    private String objectId;
    private String name;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId( String objectId ) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }
}