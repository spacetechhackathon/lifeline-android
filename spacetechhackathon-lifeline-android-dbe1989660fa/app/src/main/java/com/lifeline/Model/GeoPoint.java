package com.lifeline.Model;

import static android.R.attr.name;

/**
 * Created by jacob on 10/31/17.
 */

public class GeoPoint {
    private String objectId;
    private double lattitude;
    private double longitude;
    // geoPoint

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId( String objectId ) {
        this.objectId = objectId;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude( double lattitude ) {
        this.lattitude = lattitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude( double longitude ) {
        this.longitude = longitude;
    }
}
